package com.testapplication.mobile.taskapplication.Models;

import com.google.gson.annotations.Expose;

public class Photo {
    @Expose
    private int albumId;
    @Expose
    private int id;
    @Expose
    private String title;
    @Expose
    private String url;
    @Expose
    private String thumbnailUrl;

    //region Properties
    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
    //endregion
}
