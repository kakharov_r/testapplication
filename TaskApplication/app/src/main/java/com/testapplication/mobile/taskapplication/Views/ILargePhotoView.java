package com.testapplication.mobile.taskapplication.Views;

public interface ILargePhotoView {
    void renderPhoto(String photo);
    void renderTitle(String title);
}
