package com.testapplication.mobile.taskapplication.Views;

import com.testapplication.mobile.taskapplication.Models.Album;

import java.util.List;

public interface IAlbumView {
    void renderAlbumRecyclerView(List<Album> albums);
}
