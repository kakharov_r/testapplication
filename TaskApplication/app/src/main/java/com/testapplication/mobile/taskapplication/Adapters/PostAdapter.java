package com.testapplication.mobile.taskapplication.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.testapplication.mobile.taskapplication.Activities.CommentActivity;
import com.testapplication.mobile.taskapplication.Models.Post;
import com.testapplication.mobile.taskapplication.Models.WeatherApiModels.WeatherModel;
import com.testapplication.mobile.taskapplication.R;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Object> array;
    private Context context;
    private final static int PostType = 0, WeatherType = 1;

    public PostAdapter(List<Object> array, Context context){
        this.array = array;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (array.get(position) instanceof Post) {
            return PostType;
        } else if (array.get(position) instanceof WeatherModel) {
            return WeatherType;
        }
        return -1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        RecyclerView.ViewHolder viewHolder;
        View v;
        switch (viewType){
            case PostType:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_item, parent, false);
                viewHolder=new PostViewHolder(v);
                break;
            case WeatherType:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_item, parent, false);
                viewHolder=new WeatherViewHolder(v);
                break;
            default:
                viewHolder=null;
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int viewType = holder.getItemViewType();
        switch (viewType){
            case PostType:
                final Post post = (Post)array.get(position);
                ((PostViewHolder) holder).title.setText((post.getTitle()));
                ((PostViewHolder) holder).description.setText(post.getBody());
                ((PostViewHolder) holder).layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, CommentActivity.class);
                        intent.putExtra("postId", post.getId());
                        context.startActivity(intent);
                    }
                });
                break;
            case WeatherType:
                final WeatherModel weather = (WeatherModel) array.get(position);
                ((WeatherViewHolder)holder).city.setText(weather.getName());
                String temp = String.format("%.2f", weather.getMain().getTemp())+ " ℃";
                ((WeatherViewHolder)holder).temp.setText(temp);
                Picasso.get().load(setWeatherIcon(weather.getWeather().get(0).getId())).into(((WeatherViewHolder)holder).icon);
                ((WeatherViewHolder)holder).layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.o.kg"));
                        context.startActivity(browserIntent);
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (array == null)
            return 0;
        return array.size();
    }

    private int setWeatherIcon(int actualId){
        int id = actualId / 100;
        int icon = 0;
        switch(id) {
            case 2 : icon = R.drawable.weather_thunder;
                break;
            case 3 : icon = R.drawable.weather_drizzle;
                break;
            case 7 : icon = R.drawable.weather_foggy;
                break;
            case 8 : icon = R.drawable.weather_sunny;
                break;
            case 5 : icon = R.drawable.weather_rainy;
                break;
        }
        return icon;
    }

    class PostViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView description;
        LinearLayout layout;

        PostViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.postitem_title);
            description = (TextView) itemView.findViewById(R.id.postitem_body);
            layout = (LinearLayout) itemView.findViewById(R.id.postitem_layout);
        }
    }

    class WeatherViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView city;
        TextView temp;
        LinearLayout layout;

        WeatherViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.weather_item_icon);
            city = (TextView) itemView.findViewById(R.id.weather_item_city_name);
            temp = (TextView) itemView.findViewById(R.id.weather_item_temp);
            layout = (LinearLayout) itemView.findViewById(R.id.weather_item_layout);
        }
    }
}
