package com.testapplication.mobile.taskapplication.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.testapplication.mobile.taskapplication.Adapters.PostAdapter;
import com.testapplication.mobile.taskapplication.Models.Post;
import com.testapplication.mobile.taskapplication.Models.ViewModels.PostViewModel;
import com.testapplication.mobile.taskapplication.Models.WeatherApiModels.Main;
import com.testapplication.mobile.taskapplication.Models.WeatherApiModels.Weather;
import com.testapplication.mobile.taskapplication.Models.WeatherApiModels.WeatherModel;
import com.testapplication.mobile.taskapplication.Presenters.PostPresenter;
import com.testapplication.mobile.taskapplication.R;
import com.testapplication.mobile.taskapplication.Views.IPostView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostFragment extends Fragment implements IPostView {

    @BindView(R.id.posts_recycle_view)
    RecyclerView recyclerView;

    private List<Object> array;
    private static final String AppId = "bf2e99b430d59d2009b07f42cf1b1d74";
    private static final int Bishkek = 1528675;
    private static final int Osh = 1527534;
    private static final int Naryn = 1527592;
    private static final int CholponAta = 1528512;
    private static final String Units = "metric";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post, container, false);
        ButterKnife.bind(this, view);
        array = new ArrayList<>();
        PostPresenter postPresenter = new PostPresenter(this);
        postPresenter.GetWeather(Bishkek, AppId, Units);
        postPresenter.GetWeather(Osh, AppId, Units);
        postPresenter.GetWeather(Naryn, AppId, Units);
        postPresenter.GetWeather(CholponAta, AppId, Units);
        postPresenter.LoadPosts();
        return view;
    }

    public void renderPostRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        Collections.shuffle(array);
        PostAdapter adapter = new PostAdapter(array, getContext());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setWeather(WeatherModel weather) {
        array.add(weather);
        renderPostRecyclerView();
    }

    @Override
    public void setPost(List<Post> posts) {
        array.addAll(posts);
        renderPostRecyclerView();
    }
}
