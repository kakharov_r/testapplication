package com.testapplication.mobile.taskapplication.Views;

import com.testapplication.mobile.taskapplication.Models.Comment;

import java.util.List;

public interface ICommentView {
    void renderCommentRecyclerView(List<Comment> comments);
}
