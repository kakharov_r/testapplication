package com.testapplication.mobile.taskapplication.Presenters;

import android.support.annotation.NonNull;

import com.testapplication.mobile.taskapplication.Interfaces.Presenters.IPresenter;
import com.testapplication.mobile.taskapplication.Models.Photo;
import com.testapplication.mobile.taskapplication.Models.Post;
import com.testapplication.mobile.taskapplication.Service.Service;
import com.testapplication.mobile.taskapplication.Views.IPhotoView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotoPresenter implements IPresenter {

    private IPhotoView view;
    private int albumId;

    public PhotoPresenter(IPhotoView view, int albumId){
        this.view = view;
        this.albumId = albumId;
    }

    public void LoadPhotos(){
        Service.getApi().getPhotos(albumId).enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                view.renderImageRecyclerView(response.body());
            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {

            }
        });
    }
}
