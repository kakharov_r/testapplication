package com.testapplication.mobile.taskapplication.Views;

import com.testapplication.mobile.taskapplication.Models.Post;
import com.testapplication.mobile.taskapplication.Models.WeatherApiModels.WeatherModel;

import java.util.List;

public interface IPostView {
    //void renderPostRecyclerView(List<Post> posts);
    void setWeather(WeatherModel weather);
    void setPost(List<Post> posts);
}
