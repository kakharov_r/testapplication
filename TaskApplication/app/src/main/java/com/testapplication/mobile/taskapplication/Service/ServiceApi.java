package com.testapplication.mobile.taskapplication.Service;

import com.testapplication.mobile.taskapplication.Models.Album;
import com.testapplication.mobile.taskapplication.Models.Comment;
import com.testapplication.mobile.taskapplication.Models.Photo;
import com.testapplication.mobile.taskapplication.Models.Post;
import com.testapplication.mobile.taskapplication.Models.WeatherApiModels.WeatherModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ServiceApi {
    @GET("http://jsonplaceholder.typicode.com/posts")
    Call<List<Post>> getPosts();

    @GET("http://jsonplaceholder.typicode.com/comments")
    Call<List<Comment>> getComments(@Query("postId") int postId);

    @GET("http://jsonplaceholder.typicode.com/albums")
    Call<List<Album>> getAlbums();

    @GET("http://jsonplaceholder.typicode.com/photos")
    Call<List<Photo>> getPhotos(@Query("albumId") int albumId);

    @GET("http://jsonplaceholder.typicode.com/photos")
    Call<List<Photo>> getPhoto(@Query("id") int id);

    @GET("https://api.openweathermap.org/data/2.5/weather")
    Call<WeatherModel> getWeather(@Query("id") int id, @Query("appid") String appId, @Query("units") String units);
}
