package com.testapplication.mobile.taskapplication.Models.WeatherApiModels;

import com.google.gson.annotations.Expose;

public class Main {
    @Expose
    private double temp;
    @Expose
    private double temp_min;
    @Expose
    private double temp_max;

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public double getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(double temp_min) {
        this.temp_min = temp_min;
    }

    public double getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(double temp_max) {
        this.temp_max = temp_max;
    }
}

