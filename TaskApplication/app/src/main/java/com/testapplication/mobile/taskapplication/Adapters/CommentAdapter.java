package com.testapplication.mobile.taskapplication.Adapters;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.testapplication.mobile.taskapplication.Models.Comment;
import com.testapplication.mobile.taskapplication.R;

import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder>{

    private List<Comment> comments;

    public CommentAdapter(List<Comment> comments){
        this.comments = comments;
    }

    @NonNull
    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item, parent, false);
        return new CommentAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentAdapter.ViewHolder holder, int position) {
        Comment comment = comments.get(position);
        holder.body.setText(comment.getBody());
        holder.name.setText(comment.getName());
        holder.email.setText(comment.getEmail());
    }

    @Override
    public int getItemCount() {
        if (comments == null)
            return 0;
        return comments.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView body;
        TextView name;
        TextView email;

        ViewHolder(View itemView) {
            super(itemView);
            body = (TextView) itemView.findViewById(R.id.comment_item_body);
            name = (TextView) itemView.findViewById(R.id.comment_item_name);
            email = (TextView) itemView.findViewById(R.id.comment_item_email);
        }
    }
}
