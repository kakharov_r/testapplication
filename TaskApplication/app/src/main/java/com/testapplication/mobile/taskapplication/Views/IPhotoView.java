package com.testapplication.mobile.taskapplication.Views;

import com.testapplication.mobile.taskapplication.Models.Photo;

import java.util.List;

public interface IPhotoView {
    void renderImageRecyclerView(List<Photo> photos);
}
