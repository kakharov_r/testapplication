package com.testapplication.mobile.taskapplication.Presenters;

import android.support.annotation.NonNull;

import com.testapplication.mobile.taskapplication.Interfaces.Presenters.IPresenter;
import com.testapplication.mobile.taskapplication.Models.Post;
import com.testapplication.mobile.taskapplication.Models.WeatherApiModels.WeatherModel;
import com.testapplication.mobile.taskapplication.Service.Service;
import com.testapplication.mobile.taskapplication.Views.IPostView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostPresenter implements IPresenter {
    private IPostView view;

    public PostPresenter(IPostView view){
        this.view = view;
    }

    public void GetWeather(int id, String appId, String units){
        Service.getApi().getWeather(id, appId, units).enqueue(new Callback<WeatherModel>() {
            @Override
            public void onResponse(@NonNull Call<WeatherModel> call, @NonNull Response<WeatherModel> response) {
                view.setWeather(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<WeatherModel> call, @NonNull Throwable t) {
                String a = "";
            }
        });
    }

    public List<Post> Posts() throws IOException {
        return Service.getApi().getPosts().execute().body();
    }

    public void LoadPosts(){
        Service.getApi().getPosts().enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(@NonNull Call<List<Post>> call, @NonNull Response<List<Post>> response) {
                List<Post> posts = new ArrayList<>();
                Random random = new Random();
                for (int i = 0; i != 10; ++i){
                    posts.add(response.body().get(random.nextInt(response.body().size())));
                }
                view.setPost(posts);
            }

            @Override
            public void onFailure(@NonNull Call<List<Post>> call, @NonNull Throwable t) {

            }
        });
    }

}
