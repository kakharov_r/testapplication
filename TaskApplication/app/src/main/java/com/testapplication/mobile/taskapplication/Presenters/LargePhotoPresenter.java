package com.testapplication.mobile.taskapplication.Presenters;

import android.support.annotation.NonNull;

import com.testapplication.mobile.taskapplication.Interfaces.Presenters.IPresenter;
import com.testapplication.mobile.taskapplication.Models.Photo;
import com.testapplication.mobile.taskapplication.Service.Service;
import com.testapplication.mobile.taskapplication.Views.ILargePhotoView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LargePhotoPresenter implements IPresenter {
    private ILargePhotoView view;
    private int id;

    public LargePhotoPresenter(ILargePhotoView view, int id){
        this.view = view;
        this.id = id;
    }

    public void LoadPhoto(){
        Service.getApi().getPhoto(id).enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(@NonNull Call<List<Photo>> call, @NonNull Response<List<Photo>> response) {
                view.renderPhoto(response.body().get(0).getUrl());
                view.renderTitle(response.body().get(0).getTitle());
            }

            @Override
            public void onFailure(@NonNull Call<List<Photo>> call, @NonNull Throwable t) {

            }
        });
    }
}
