package com.testapplication.mobile.taskapplication.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.testapplication.mobile.taskapplication.Adapters.AlbumAdapter;
import com.testapplication.mobile.taskapplication.Adapters.CommentAdapter;
import com.testapplication.mobile.taskapplication.Models.Comment;
import com.testapplication.mobile.taskapplication.Presenters.CommentPresenter;
import com.testapplication.mobile.taskapplication.R;
import com.testapplication.mobile.taskapplication.Views.ICommentView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentActivity extends AppCompatActivity implements ICommentView{

    @BindView(R.id.comments_recycle_view)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ButterKnife.bind(this);
        setTitle("Comments");
        int postId = getIntent().getIntExtra("postId", 0);
        CommentPresenter presenter =  new CommentPresenter(this, postId);
        presenter.LoadComments();
    }

    @Override
    public void renderCommentRecyclerView(List<Comment> comments) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        CommentAdapter adapter = new CommentAdapter(comments);
        recyclerView.setAdapter(adapter);
    }
}
