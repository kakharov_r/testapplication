package com.testapplication.mobile.taskapplication.Models.WeatherApiModels;

import com.google.gson.annotations.Expose;

import java.util.List;

public class WeatherModel {
    @Expose
    private List<Weather> weather;
    @Expose
    private Main main;
    @Expose
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }
}
