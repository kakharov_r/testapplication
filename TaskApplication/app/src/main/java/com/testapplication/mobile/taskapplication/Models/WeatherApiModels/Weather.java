package com.testapplication.mobile.taskapplication.Models.WeatherApiModels;

import com.google.gson.annotations.Expose;

public class Weather {
    @Expose
    private int id;
    @Expose
    private String main;
    @Expose
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
