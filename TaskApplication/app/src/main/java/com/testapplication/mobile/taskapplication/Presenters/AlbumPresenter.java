package com.testapplication.mobile.taskapplication.Presenters;

import android.support.annotation.NonNull;

import com.testapplication.mobile.taskapplication.Interfaces.Presenters.IPresenter;
import com.testapplication.mobile.taskapplication.Models.Album;
import com.testapplication.mobile.taskapplication.Service.Service;
import com.testapplication.mobile.taskapplication.Views.IAlbumView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlbumPresenter implements IPresenter {
    IAlbumView view;

    public AlbumPresenter(IAlbumView view){
        this.view = view;
    }

    public void LoadAlbums(){
        Service.getApi().getAlbums().enqueue(new Callback<List<Album>>() {
            @Override
            public void onResponse(@NonNull Call<List<Album>> call, @NonNull Response<List<Album>> response) {
                List<Album> albums = new ArrayList<>();
                Random random = new Random();
                for (int i = 0; i != 10; ++i){
                    albums.add(response.body().get(random.nextInt(response.body().size())));
                }
                view.renderAlbumRecyclerView(albums);
            }

            @Override
            public void onFailure(@NonNull Call<List<Album>> call, Throwable t) {

            }
        });
    }
}
