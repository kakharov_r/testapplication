package com.testapplication.mobile.taskapplication.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;

import com.testapplication.mobile.taskapplication.Fragments.AlbumFragment;
import com.testapplication.mobile.taskapplication.Fragments.PostFragment;
import com.testapplication.mobile.taskapplication.R;

public class PageAdapter extends FragmentStatePagerAdapter {

    private static final int NUM_PAGES = 2;
    private Context context;

    String[] titles = {"Posts", "Albums"};

    public void setContext(Context context) {
        this.context = context;
    }

    public PageAdapter(FragmentManager fragmentManager){
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new PostFragment();
            case 1:
                return new AlbumFragment();
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
