package com.testapplication.mobile.taskapplication.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.testapplication.mobile.taskapplication.Activities.LargePhotoActivity;
import com.testapplication.mobile.taskapplication.Activities.PhotoActivity;
import com.testapplication.mobile.taskapplication.CustomViews.SquareFrameLayout;
import com.testapplication.mobile.taskapplication.Models.Photo;
import com.testapplication.mobile.taskapplication.Presenters.LargePhotoPresenter;
import com.testapplication.mobile.taskapplication.R;

import java.util.List;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolder> {

    private List<Photo> photos;
    private Context context;

    public PhotoAdapter(List<Photo> photos, Context context){
        this.photos = photos;
        this.context = context;
    }

    @NonNull
    @Override
    public PhotoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_item, parent, false);
        return new PhotoAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoAdapter.ViewHolder holder, int position) {
        final Photo photo = photos.get(position);
        Picasso.get().load(photo.getThumbnailUrl()).into(holder.photo);
        holder.frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LargePhotoActivity.class);
                intent.putExtra("id", photo.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (photos == null)
            return 0;
        return photos.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView photo;
        SquareFrameLayout frameLayout;

        ViewHolder(View itemView) {
            super(itemView);
            photo = (ImageView) itemView.findViewById(R.id.image_item_photo);
            frameLayout = (SquareFrameLayout) itemView.findViewById(R.id.photo_item_layout);
        }
    }
}
