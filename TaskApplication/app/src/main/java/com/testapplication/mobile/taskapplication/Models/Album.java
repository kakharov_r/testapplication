package com.testapplication.mobile.taskapplication.Models;

import com.google.gson.annotations.Expose;

public class Album {
    @Expose
    private int userId;
    @Expose
    private int id;
    @Expose
    private String title;

    //region Properties
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    //endregion
}
