package com.testapplication.mobile.taskapplication.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.testapplication.mobile.taskapplication.Presenters.LargePhotoPresenter;
import com.testapplication.mobile.taskapplication.R;
import com.testapplication.mobile.taskapplication.Views.ILargePhotoView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LargePhotoActivity extends AppCompatActivity implements ILargePhotoView{

    @BindView(R.id.large_photo_image_view)
    ImageView imageView;

    @BindView(R.id.large_photo_title)
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_large_photo);
        setTitle("Photo");
        ButterKnife.bind(this);
        int id = getIntent().getIntExtra("id", 0);
        LargePhotoPresenter presenter = new LargePhotoPresenter(this, id);
        presenter.LoadPhoto();
    }

    @Override
    public void renderPhoto(String photo) {
        Picasso.get().load(photo).into(imageView);
    }

    @Override
    public void renderTitle(String title) {
        textView.setText(title);
    }
}
