package com.testapplication.mobile.taskapplication.Models.ViewModels;

public class PostViewModel {
    private int userId;
    private int id;
    private String title;
    private String body;

    public PostViewModel(int id, String main, double temp, double temp_min, double temp_max, String name, boolean isPost) {
        this.id = id;
        this.main = main;
        this.temp = temp;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
        this.name = name;
        this.isPost = isPost;
    }

    public PostViewModel(int userId, int id, String title, String body, boolean isPost) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.body = body;
        this.isPost = isPost;

    }

    private String main;
    private double temp;
    private double temp_min;
    private double temp_max;
    private String name;

    private boolean isPost;

    public boolean isPost() {
        return isPost;
    }

    public void setPost(boolean post) {
        isPost = post;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public double getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(double temp_min) {
        this.temp_min = temp_min;
    }

    public double getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(double temp_max) {
        this.temp_max = temp_max;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
