package com.testapplication.mobile.taskapplication.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.testapplication.mobile.taskapplication.Activities.CommentActivity;
import com.testapplication.mobile.taskapplication.Activities.PhotoActivity;
import com.testapplication.mobile.taskapplication.CustomViews.SquareFrameLayout;
import com.testapplication.mobile.taskapplication.Models.Album;
import com.testapplication.mobile.taskapplication.R;

import java.util.List;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder>{

    private List<Album> albums;
    private Context context;

    public AlbumAdapter(List<Album> albums, Context context){
        this.albums = albums;
        this.context = context;
    }

    @NonNull
    @Override
    public AlbumAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_item, parent, false);
        return new AlbumAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumAdapter.ViewHolder holder, int position) {
        final Album album = albums.get(position);
        holder.title.setText((album.getTitle()));
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PhotoActivity.class);
                intent.putExtra("albumId", album.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (albums == null)
            return 0;
        return albums.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        SquareFrameLayout layout;

        ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.album_item_title);
            layout = (SquareFrameLayout) itemView.findViewById(R.id.album_item_frame);
        }
    }
}
