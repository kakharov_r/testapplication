package com.testapplication.mobile.taskapplication.Presenters;

import android.support.annotation.NonNull;

import com.testapplication.mobile.taskapplication.Interfaces.Presenters.IPresenter;
import com.testapplication.mobile.taskapplication.Models.Album;
import com.testapplication.mobile.taskapplication.Models.Comment;
import com.testapplication.mobile.taskapplication.Service.Service;
import com.testapplication.mobile.taskapplication.Views.ICommentView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentPresenter implements IPresenter {

    private ICommentView view;
    private int postId;

    public CommentPresenter(ICommentView view, int postId){
        this.view = view;
        this.postId = postId;
    }

    public void LoadComments(){
        Service.getApi().getComments(postId).enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                view.renderCommentRecyclerView(response.body());
            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {

            }
        });
    }
}
