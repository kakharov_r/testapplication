package com.testapplication.mobile.taskapplication.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.testapplication.mobile.taskapplication.Adapters.AlbumAdapter;
import com.testapplication.mobile.taskapplication.Models.Album;
import com.testapplication.mobile.taskapplication.Presenters.AlbumPresenter;
import com.testapplication.mobile.taskapplication.R;
import com.testapplication.mobile.taskapplication.Views.IAlbumView;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlbumFragment extends Fragment implements IAlbumView{

    @BindView(R.id.albums_recycle_view)
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_album, container, false);
        ButterKnife.bind(this,view);
        AlbumPresenter albumPresenter = new AlbumPresenter(this);
        albumPresenter.LoadAlbums();
        return view;
    }

    @Override
    public void renderAlbumRecyclerView(List<Album> albums) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        AlbumAdapter adapter = new AlbumAdapter(albums, getContext());
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        recyclerView.setAdapter(adapter);
    }
}
