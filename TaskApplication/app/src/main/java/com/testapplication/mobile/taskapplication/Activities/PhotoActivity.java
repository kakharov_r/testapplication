package com.testapplication.mobile.taskapplication.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.testapplication.mobile.taskapplication.Adapters.PhotoAdapter;
import com.testapplication.mobile.taskapplication.Models.Photo;
import com.testapplication.mobile.taskapplication.Presenters.PhotoPresenter;
import com.testapplication.mobile.taskapplication.R;
import com.testapplication.mobile.taskapplication.Views.IPhotoView;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotoActivity extends AppCompatActivity implements IPhotoView {

    @BindView(R.id.images_recycle_view)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        ButterKnife.bind(this);
        setTitle("Photos");
        int albumId = getIntent().getIntExtra("albumId", 0);
        PhotoPresenter presenter = new PhotoPresenter(this, albumId);
        presenter.LoadPhotos();
    }

    @Override
    public void renderImageRecyclerView(List<Photo> photos) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        PhotoAdapter adapter = new PhotoAdapter(photos, this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setAdapter(adapter);
    }
}
